package sk.murin.weatherweb.model.weather.current

import com.fasterxml.jackson.annotation.JsonProperty

class WeatherResponse(
    val visibility: Double,
    val base: String,
    @field:JsonProperty("dt") val dateTime: Long,
    val timezone: Long,
    val id: Int,
    val name: String,
    val cod: Int,
    val coord: WeatherCoordinates,

    val weather: List<Weather>,
    val main: WeatherMain,
    val wind: WeatherWind,
    val clouds: WeatherClouds,


    @JsonProperty("sys")
//takto sa to vola v api v jsone na stranke odkial to bereme
    val weatherSys: WeatherSys
)

