package sk.murin.weatherweb.model.weather.daily

data class DailyFeelsLike(
    val day: Double,
    val night: Double,
    val eve: Double,
    val morn: Double,
)