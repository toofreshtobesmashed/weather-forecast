package sk.murin.weatherweb.model.weather.current

class WeatherCoordinates(val lon: Double,
                         val lat: Double)