package sk.murin.weatherweb.model.weather.current

class WeatherSys(
    val type: Int,
    val id: Int,
    val country: String,
    val sunrise: Long,
    val sunset: Long
)