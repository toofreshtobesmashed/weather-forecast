package sk.murin.weatherweb.model.weather.daily

import com.fasterxml.jackson.annotation.JsonProperty

data class  DailyForecast(
    @JsonProperty("dt")
    val dateTime: Long,
    val sunrine: Long,
    val sunset: Long,
    val moonrise: Long,
    val moonset: Long,
    @JsonProperty("moon_phase")
    val moonPhase: Float,
    val temp: DailyForecastTemp,
    @JsonProperty("feels_like")
    val tempFeelsLike: DailyFeelsLike,
    val pressure: Int,
    val humidity: Int,
    @JsonProperty("dew_point")
    val dewPoint: Double,
    @JsonProperty("wind_speed")
    val windSpeed: Double,
    @JsonProperty("wind_deg")
    val windDeg: Double,
    @JsonProperty("wind_gust")
    val windGust: Double,
    val weather: List<Weather>,
    val clouds: Int,
    val pop: Int,
    @JsonProperty("uvi")
    val uvIndex: Double
)