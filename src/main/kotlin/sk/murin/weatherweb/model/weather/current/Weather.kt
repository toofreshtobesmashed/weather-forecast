package sk.murin.weatherweb.model.weather.current

class Weather(val id: Int,
              val main: String,
              val description: String,
              val icon: String)