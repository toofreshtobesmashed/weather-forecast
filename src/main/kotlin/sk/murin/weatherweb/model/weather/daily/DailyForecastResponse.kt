package sk.murin.weatherweb.model.weather.daily

import com.fasterxml.jackson.annotation.JsonProperty

data class  DailyForecastResponse(
    val lat: Double,
    val lon: Double,
    val timezone: String,
    @JsonProperty("timezone_offset")
    val timeZoneOffset: Long,
    val daily: List<DailyForecast>


)
