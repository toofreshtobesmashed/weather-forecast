package sk.murin.weatherweb.model.weather.current

class WeatherWind(
    val speed: Double,
    val deg: Double,
    val gust: Double
)