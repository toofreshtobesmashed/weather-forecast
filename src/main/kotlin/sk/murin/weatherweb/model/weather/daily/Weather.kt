package sk.murin.weatherweb.model.weather.daily

data class  Weather(
    val id: Int,
    val main: String,
    val description: String,
    val icon: String
)