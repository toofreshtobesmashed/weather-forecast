package sk.murin.weatherweb.model.weather.current

class WeatherMain(
    val feels_like: Double,
    val temp_min: Double,
    val temp_max: Double,
    val pressure: Double,
    val humidity: Double
) {

    var temp = 0.0
        get() = field.minus(273.15).times(100.0).toInt().div(100.0)


}