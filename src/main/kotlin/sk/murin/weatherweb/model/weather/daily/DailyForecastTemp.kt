package sk.murin.weatherweb.model.weather.daily

data class  DailyForecastTemp(
    val day: Double,
    val min: Double,
    val max: Double,
    val night: Double,
    val eve: Double,
    val morn: Double,


    )