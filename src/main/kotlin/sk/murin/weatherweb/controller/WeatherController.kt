package sk.murin.weatherweb.controller

import org.springframework.stereotype.Controller
import org.springframework.ui.Model
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import sk.murin.weatherweb.extensions.toCelsius
import sk.murin.weatherweb.extensions.toDegreesString
import sk.murin.weatherweb.service.WeatherService
import java.text.SimpleDateFormat
import java.util.*

@Controller
class WeatherController(val weatherService: WeatherService) {


    @GetMapping
    fun refresh(model: Model) = showData("Bratislava", "Liptovsky Mikulas", "Tokyo", model)

    companion object {
        val dateFormat = SimpleDateFormat("EEEE d. M. HH:mm:ss")
        val forecastDateFormat = SimpleDateFormat("EEEE d. M.")
        const val ICONS_BASE_URL = "http://openweathermap.org/img/wn/"
    }


    @GetMapping("/{query1}/{query2}/{query3}")
    fun showData(
        @PathVariable query1: String, @PathVariable query2: String,
        @PathVariable query3: String, model: Model
    ): String {

        val cities = listOf(query1, query2, query3)
        val models = mutableListOf<WeatherViewModel>()

        for (city in cities) {
            val current = weatherService.getCurrentData(city) ?: return "error"
            val forecast = weatherService.getForecastData(current.coord.lat, current.coord.lon) ?: return "error"
            val forecastViewModels = mutableListOf<WeatherViewModel.ForecastViewModel>()

            forecast.daily.forEach {
                forecastViewModels.add(
                    WeatherViewModel.ForecastViewModel(
                        forecastDateFormat.format(getDate(it.dateTime,forecast.timeZoneOffset)),
                        it.temp.min.toCelsius().toDegreesString(),
                        it.temp.max.toCelsius().toDegreesString()
                    )
                )
            }

            val weatherViewModel = WeatherViewModel(
                current.name,
                current.main.temp,
                current.weather[0].description,
                dateFormat.format(getDate(current.dateTime, current.timezone)),
                current.wind.speed,
                "$ICONS_BASE_URL${current.weather.first().icon}@4x.png",
                dateFormat.format(getDate(current.weatherSys.sunrise, current.timezone)),
                dateFormat.format(getDate(current.weatherSys.sunset, current.timezone)),
                forecastViewModels

            )
            models.add(weatherViewModel)
        }
        model.addAttribute("models", models)
        return "index"
    }

    private fun getDate(dateTime: Long, timezone: Long) =
        Date((dateTime + timezone) * 1000 - TimeZone.getDefault().rawOffset)


}