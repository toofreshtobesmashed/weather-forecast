package sk.murin.weatherweb.controller


class WeatherViewModel(
    val city: String,
    val temperature: Double,
    val sky: String,
    val time: String,
    val wind: Double,
    val iconUrl: String,
    val sunrise: String,
    val sunset: String,
    val forecastViewModels: List<ForecastViewModel>,



) {


    class ForecastViewModel(
        val date: String,
        val minTemp: String,
        val maxTemp: String,
        // val sky: String
    )

}




