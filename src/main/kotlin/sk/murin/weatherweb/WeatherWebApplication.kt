package sk.murin.weatherweb

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WeatherWebApplication

fun main(args: Array<String>) {
    runApplication<WeatherWebApplication>(*args)
}
