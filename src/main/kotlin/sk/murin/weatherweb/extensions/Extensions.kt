package sk.murin.weatherweb.extensions

fun Double.toCelsius() = this - 273.15

fun Double.toDegreesString()=String.format("%.2f°C",this)