package sk.murin.weatherweb.service

import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import sk.murin.weatherweb.model.weather.current.WeatherResponse
import sk.murin.weatherweb.model.weather.daily.DailyForecastResponse

@Service
class WeatherService {
//ba 48.1214877,17.0726511
    companion object {
        private const val API_KEY = "955643da8a4f5cf9320b6ce15f5091ed"
        const val BASE_URL = "https://api.openweathermap.org/data/2.5"

    }

    fun getCurrentData(query: String): WeatherResponse? {
        val url = "$BASE_URL/weather?appid=$API_KEY&q=$query&lang=sk"
        val restTemplate = RestTemplate()
        return restTemplate.getForObject(url, WeatherResponse::class.java)
    }

    fun getForecastData(lat:Double,lon:Double):DailyForecastResponse? {
        val url = "$BASE_URL/onecall?appid=$API_KEY&lat=$lat&lon=$lon&exclude=current,minutely,hourly,alerts&lang=sk"
        val restTemplate = RestTemplate()
        return restTemplate.getForObject(url, DailyForecastResponse::class.java)
    }




}