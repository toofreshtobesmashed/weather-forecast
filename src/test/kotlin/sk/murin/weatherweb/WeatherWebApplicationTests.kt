package sk.murin.weatherweb

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import sk.murin.weatherweb.service.WeatherService
import java.util.*

@SpringBootTest
class WeatherWebApplicationTests(@Autowired val weatherService: WeatherService) {


    @Test
    fun `checking link`() {
        //  println(weatherService.getData("Tokyo"))

        val info = weatherService.getCurrentData("Liptovsky Mikulas") ?: return
        // weatherService.getData("Bratislava")
        val sky = info.weather[0].description
        println(sky)

        val temperature = info.main.temp
        println("$temperature °C")

        val wind = info.wind.speed
        println(wind)

        val time = Date(info.dateTime * 1000)
        println(time)

        val city = info.name
        println(city)

        val finalResult = "$sky $temperature $wind $time $city"
        println(finalResult)

    }

    @Test
    fun `checking forecast`() {

        val info = weatherService.getForecastData(48.1214877, 17.0726511) ?: return//ba
        println(info)

















    }


}
